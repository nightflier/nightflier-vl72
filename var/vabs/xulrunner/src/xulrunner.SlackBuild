#!/bin/sh
# $Id: xulrunner.SlackBuild,v 1.17 2011/05/13 11:27:33 root Exp root $
# Copyright 2009, 2010, 2011  Eric Hameleers, Eindhoven, NL
# All rights reserved.
#
#   Permission to use, copy, modify, and distribute this software for
#   any purpose with or without fee is hereby granted, provided that
#   the above copyright notice and this permission notice appear in all
#   copies.
#
#   THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
#   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#   MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
#   IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
#   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
#   USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
#   OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
#   SUCH DAMAGE.
# -----------------------------------------------------------------------------
#
# Slackware SlackBuild script 
# ===========================
# By:         Eric Hameleers <alien@slackware.com>
# For:        xulrunner
# Descr:      mozilla runtime package
# URL:        https://developer.mozilla.org/en/XULRunner
# Needs:      java (only build-time)
# Changelog:  
# 1.9.1.4-1:  12/Nov/2009 by Eric Hameleers <alien@slackware.com>
#             * Initial build.
# 1.9.2.9-1:  15/sep/2010 by Eric Hameleers <alien@slackware.com>
#             * Update.
# 1.9.2.10-1: 11/oct/2010 by Eric Hameleers <alien@slackware.com>
#             * Update.
# 1.9.2.12-1: 07/nov/2010 by Eric Hameleers <alien@slackware.com>
#             * Update. Use xz compression to reduce package size.
#               Add xpcom. Provide more pkg-config symlinks, I want to be able
#               to remove seamonkey entirely.
# 1.9.2.17-1: 13/may/2011 by Eric Hameleers <alien@slackware.com>
#             * Update.
# 
# Run 'sh xulrunner.SlackBuild' to build a Slackware package.
# The package (.txz) and .txt file as well as build logs are created in /tmp .
# Install it using 'installpkg'. 
#
# -----------------------------------------------------------------------------

# Set initial variables:

NAME=xulrunner
VERSION=${VERSION:-"15.0.1"}
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-"cairo pixman nspr mozilla-nss libev libvpx sqlite libpng infozip hunspell libnotify dbus-glib yasm alsa-lib wireless-tools"}

DOCS="LEGAL LICENSE README.txt"
SRCVER=$(echo $VERSION | cut -d. -f1-3)
# Where do we look for sources?
SRCDIR=$(cd $(dirname $0); pwd)
PRGNAM=$NAME
# Place to build (TMP) package (PKG) and output (OUTPUT) the program:
TMP=${TMP:-/tmp/build}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-"$SRCDIR/.."}

SOURCE="$SRCDIR/${PRGNAM}-${VERSION}.source.tar.bz2"
SRCURL="http://ftp.mozilla.org/pub/mozilla.org/${NAME}/releases/${VERSION}/source/${NAME}-${VERSION}.source.tar.bz2"
LINK=$SRCURL
#CFLAGS SETUP
#--------------------------------------------
if [[ "$ARCH" = i?86 ]]; then
  ARCH=i586
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i586-vector-linux"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fpic"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "powerpc" ]; then
  SLKCFLAGS="-O2"
  CONFIGURE_TRIPLET="powerpc-vlocity-linux"
  LIBDIRSUFFIX=""
fi

export CFLAGS="$SLKCFLAGS $CFLAG_OPTIONS"
export CXXFLAGS=$CFLAGS
export LDFLAGS="$LDFLAGS $LDFLAG_OPTIONS"
#--------------------------------------------

# DO NOT EXECUTE if NORUN is set to 1
if [ "$NORUN" != "1" ]; then
##
## --- with a little luck, you won't have to edit below this point --- ##
##
# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) export ARCH=i?86 ;;
    arm*) export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) export ARCH=$( uname -m ) ;;
  esac
fi

case "$ARCH" in
  i?86)      SLKCFLAGS="-O2 -march=i486 -mtune=i686"
             SLKLDFLAGS=""; LIBDIRSUFFIX=""
             ;;
  x86_64)    SLKCFLAGS="-O2 -fPIC"
             SLKLDFLAGS="-L/usr/lib64"; LIBDIRSUFFIX="64"
             ;;
  *)         SLKCFLAGS="-O2"
             SLKLDFLAGS=""; LIBDIRSUFFIX=""
             ;;
esac

# Exit the script on errors:
#set -e
trap 'echo "$0 FAILED at line ${LINENO}" | tee $OUTPUT/error-${PRGNAM}.log' ERR
# Catch unitialized variables:
set -u
P1=${1:-1}

# Save old umask and set to 0022:
_UMASK_=$(umask)
umask 0022

# Create working directories:
mkdir -p $OUTPUT          # place for the package to be saved
mkdir -p $TMP/tmp-$PRGNAM # location to build the source
mkdir -p $PKG             # place for the package to be built
rm -rf $PKG/*             # always erase old package's contents
rm -rf $TMP/tmp-$PRGNAM/* # remove the remnants of previous build
rm -rf $OUTPUT/{configure,make,install,error,makepkg}-$PRGNAM.log
                          # remove old log files

# Source file availability:
if ! [ -f ${SOURCE} ]; then
  if ! [ "x${SRCURL}" == "x" ]; then
    # Check if the $SRCDIR is writable at all - if not, download to $OUTPUT
    [ -w "$SRCDIR" ] || SOURCE="$OUTPUT/$(basename $SOURCE)"
    echo "Source '$(basename ${SOURCE})' not available yet..."
    echo "Will download file to $(dirname $SOURCE)"
    wget -c -T 20 -O "${SOURCE}" "${SRCURL}" || true
    if [ $? -ne 0 -o ! -s "${SOURCE}" ]; then
      echo "Downloading '$(basename ${SOURCE})' failed... aborting the build."
      mv -f "${SOURCE}" "${SOURCE}".FAIL
      exit 1
    fi
  else
    echo "File '$(basename ${SOURCE})' not available... aborting the build."
    exit 1
  fi
fi

if [ "$P1" == "--download" ]; then
  echo "Download complete."
  exit 0
fi

# --- PACKAGE BUILDING ---

echo "++"
echo "|| $PRGNAM-$VERSION"
echo "++"

cd $TMP/tmp-$PRGNAM
echo "Extracting the source archive(s) for $PRGNAM..."
# Spare the looooooooong output during decompression
tar xf ${SOURCE} --checkpoint=10000 || exit 1
mv * $PRGNAM-$VERSION
cd $PRGNAM-$VERSION
chown -R root:root .
chmod -R u+w,go+r-w,a+X-s .

#fix libdir/sdkdir - fedora
patch -Np1 -i $SRCDIR/patches/mozilla-pkgconfig.patch --verbose || exit 1
patch -Np1 -i $SRCDIR/patches/shared-libs.patch --verbose || exit 1
#patch -Np1 -i $SRCDIR/mozilla-nss.patch --verbose || exit 1
patch -Np0 -i $SRCDIR/patches/disable-errors-as-warnings.diff --verbose || exit 1
patch -Np1 -i $SRCDIR/patches/freetype-headers.patch --verbose || exit 1
#patch -p1 < $SRCDIR/xulrunner-13.0-fix_build_idl_parser.patch
#if gcc --version | grep -q "gcc (GCC) 4.7.0" ; then 
 # Enable compiling with gcc-4.7.0:
 sed -i '/fcntl.h/a#include <unistd.h>' \
   ipc/chromium/src/base/{file_util_linux,message_pump_libevent,process_util_posix}.cc &&
 sed -i '/sys\/time\.h/a#include <unistd.h>' ipc/chromium/src/base/time_posix.cc &&
 sed -i 's#\"PRIxPTR#\" PRIxPTR#' layout/base/tests/TestPoisonArea.cpp &&
 sed -i 's# ""##' browser/base/Makefile.in
#fi
#patch -p1 <$SRCDIR/patches/xulrunner-version-script.patch
patch -p1 <$SRCDIR/patches/xulrunner-cairo.patch
echo Building ...
export LDFLAGS="$SLKLDFLAGS"
export CXXFLAGS="$SLKCFLAGS"
export CFLAGS="$SLKCFLAGS"
#find . -type f -exec sed -i 's|-Werror||g' {} \;
#autoreconf -aiv || exit 1
#autoconf || exit 1
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --with-system-nspr \
  --with-system-nss \
  --with-system-jpeg \
  --with-system-zlib \
  --with-system-bz2 \
  --with-system-png \
  --with-system-libevent \
  --with-system-libvpx \
  --enable-system-hunspell \
  --enable-system-sqlite \
  --enable-system-ffi \
  --enable-system-cairo \
  --enable-system-pixman --with-pthreads \
  --enable-safe-browsing \
  --enable-warnings-as-errors=no \
  --disable-warnings-as-errors \
  --enable-startup-notification --enable-gio \
  --mandir=/usr/man \
  --localstatedir=/var \
  --sysconfdir=/etc \
  --with-default-mozilla-five-home=/usr/lib${LIBDIRSUFFIX}/xulrunner-$VERSION \
  --enable-application=xulrunner \
  --enable-default-toolkit=cairo-gtk2 \
  --enable-javaxpcom \
  --enable-optimize="$CFLAGS" \
  --enable-strip \
  --enable-system-lcms \
  --disable-gnomevfs \
  --disable-crashreporter \
  --disable-updater \
  --disable-tests \
  --disable-mochitest \
  --disable-debug \
  --disable-installer \
  --disable-mailnews \
  --disable-pedantic \
  --program-prefix= \
  --program-suffix= \
  --build=$CONFIGURE_TRIPLET || exit 1
#  2>&1 | tee $OUTPUT/configure-${PRGNAM}.log
  # Slackware's sqlite is too old:
  #--enable-system-sqlite \
  # We use the nss/nspr of xulrunner instead, so that we can remove seamonkey:
  #--with-system-nspr \
  #--with-system-nss \

make $NUMJOBS || exit 1 #make MOZ_MAKE_FLAGS="$NUMJOBS"
make DESTDIR=$PKG install || exit 1 #2>&1 | tee $OUTPUT/install-${PRGNAM}.log

# Make sure gre configuration files do not overwrite one another on multilib,
# the filename is not important, all *.conf files in the directory are parsed:
GECKOVERS=$(./config/milestone.pl --topsrcdir=.)
mv $PKG/etc/gre.d/${GECKOVERS}.system.conf $PKG/etc/gre.d/gre${LIBDIRSUFFIX}.conf

# Add missing xulrunner pkg-config files:
( cd $PKG/usr/lib${LIBDIRSUFFIX}/pkgconfig
  for FILE in gtkmozembed-embedding gtkmozembed js nspr nss plugin xpcom ; do
    ln -sf mozilla-${FILE}.pc xulrunner-${FILE}.pc
  done
)

# Add links to the dynamic libraries in the SDK directory:
( cd $PKG/usr/lib${LIBDIRSUFFIX}/xulrunner-${VERSION}
  for i in *.so; do
    ln -sf /usr/lib${LIBDIRSUFFIX}/xulrunner-${VERSION}/$i $PKG/usr/lib${LIBDIRSUFFIX}/xulrunner-devel-${VERSION}/sdk/lib/$i
  done
)

# Make symlinks to prevent version information from entering dependencies:
( cd $PKG/usr/lib${LIBDIRSUFFIX}
  ln -sf xulrunner-${VERSION} xulrunner
  ln -sf xulrunner-devel-${VERSION} xulrunner-devel
)

# Add the following to doinst.sh:
mkdir -p $PKG/install
cat <<EOT >> $PKG/install/doinst.sh
# Ensure that xulrunner libraries are found by applications that need them
# (this way you can remove seamonkey completely):
if ! grep -q /usr/lib${LIBDIRSUFFIX}/xulrunner etc/ld.so.conf ; then
  echo "/usr/lib${LIBDIRSUFFIX}/xulrunner" >> etc/ld.so.conf
fi
EOT

# Add documentation:
mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a $DOCS $PKG/usr/doc/$PRGNAM-$VERSION || true
cat $SRCDIR/$(basename $0) > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild
chown -R root:root $PKG/usr/doc/$PRGNAM-$VERSION
find $PKG/usr/doc -type f -exec chmod 644 {} \;

# Compress the man page(s):
if [ -d $PKG/usr/man ]; then
  find $PKG/usr/man -type f -name "*.?" -exec gzip -9f {} \;
  for i in $(find $PKG/usr/man -type l -name "*.?") ; do ln -s $( readlink $i ).gz $i.gz ; rm $i ; done
fi

# Add a package description:
mkdir -p $PKG/install
cat $SRCDIR/slack-desc > $PKG/install/slack-desc
cat $SRCDIR/slack-desc > $OUTPUT/slack-desc

# Build the package:
cd $PKG
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $OUTPUT $PKG
echo "Creating package $NAME-$VERSION-$ARCH-$BUILD.txz"
makepkg --linkadd y --chown n $OUTPUT/${PRGNAM}-${VERSION}-${ARCH}-${BUILD}.txz || exit 1

# Restore the original umask:
umask ${_UMASK_}
fi
