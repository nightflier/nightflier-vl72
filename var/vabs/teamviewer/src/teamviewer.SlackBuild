#!/usr/bin/bash
# This script assumes it will be launched within "/NAME/VERSION/src" dir.
# With all sources in "src" Your Vector Linux .txz package, slack-desc,
# and slack-required will be found in "VERSION" dir. The extraction and
# build will be in a temp dir created in "NAME" dir, and then removed on exit.
# Comment out second to last line to keep this dir intact.
#
# This Template was compiled from the contributions of many users of the Vector
# Linux forum at http://forum.vectorlinux.com and from tidbits collected 
# from all over the internet. 
#
# Generated by sbbuilder-0.4.15, written by Rodrigo Bistolfi 
# (rbistolfi) and Raimon Grau Cuscó (Kidd) for VectorLinux.
#
# Please put your name below if you add some original scripting lines.
# AUTHORS = 
##Thanks to the people at http://slackbuilds.org for the references drawn from thier SlackBuild.

NAME="teamviewer"            #Enter package Name!
VERSION=${VERSION:-"9.0.27891"}      #Enter package Version!
VER=$(echo ${VERSION}|sed 's/-/_/') #this fixes - in version
VL_PACKAGER=${VL_PACKAGER:-"stretchedthin"}   #Enter your Name!
LINK1=${LINK1:-"http://download.${NAME}.com/download/${NAME}_linux.deb"}  #Enter URL for package here!
LINK2=${LINK2:-"http://download.${NAME}.com/download/${NAME}_linux_x64.deb"}  #Enter URL for package here!

#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-"ncompress util-linux wine "} #Add deps needed TO BUILD this package here.
#----------------------------------------------------------------------------

# DO NOT EXECUTE if NORUN is set to 1
if [ "$NORUN" != "1" ]; then

#SETUP PACKAGING ENVIRONMENT
#--------------------------------------------
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-${NAME}
#--------------------------------------------



if [ $UID != 0 ]; then
   echo "You are not authorized to run this script. Please login as root"
   exit 1
fi

if [ ! -x /usr/bin/requiredbuilder ]; then
   echo "Requiredbuilder not installed, or not executable."
   exit 1
fi

if [ $VL_PACKAGER = "YOURNAME" ]; then
   echo 'Who are you?
   Please edit VL_PACKAGER=${VL_PACKAGER:-YOURNAME} in this script.
   Change the word "YOURNAME" to your VectorLinux packager name.
   You may also export VL_PACKAGER, or call this script with
   VL_PACKAGER="YOUR NAME HERE"'
   exit 1
fi


#CFLAGS SETUP
#--------------------------------------------
if [[ "$ARCH" = i?86 ]]; then
  ARCH=i586
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i486-vector-linux"
  LIBDIRSUFFIX=""
  LINK=$LINK1
    DEBARCH=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fpic"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
  LIBDIRSUFFIX="64"
  DEBARCH="_x64"
  LINK=$LINK1
elif [ "$ARCH" = "powerpc" ]; then
  SLKCFLAGS="-O2"
  CONFIGURE_TRIPLET="powerpc-vlocity-linux"
  LIBDIRSUFFIX=""
  echo "Package for $(uname -m) architecture is not available."
  exit 1
fi

export CFLAGS="$SLKCFLAGS $CFLAG_OPTIONS"
export CXXFLAGS=$CFLAGS
export LDFLAGS="$LDFLAGS $LDFLAG_OPTIONS"
#--------------------------------------------


#GET THE SOURCE
#--------------------------------------------
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
        wget --no-check-certificate -c $SRC
fi
done
#--------------------------------------------
# Get the real version
REAL_VER=$(ar p teamviewer_linux${DEBARCH}.deb control.tar.gz | tar xzO ./control | grep Version | cut -d\  -f2 | cut -d- -f1)
if [ "$VERSION" != "$REAL_VER" ]; then
  echo "Version of downloaded source [$REAL_VER] does not match version of SlackBuild [$VERSION]"
  exit 1
fi

rm -rf $PKG
cd $TMP
rm -rf ${NAME}-${VERSION}


#EXTRACT SOURCES
#-----------------------------------------------------
echo "Extracting source..."

mkdir -p $PKG
cd $PKG
ar p $CWD/teamviewer_linux${DEBARCH}.deb data.tar.gz | gzip -d | tar xv
#-----------------------------------------------------




#PATCHES
#-----------------------------------------------------
# Put any Patches here *NOTE this only works if all 
# your patches use the -p1 strip option!
#-----------------------------------------------------
for i in $CWD/patches/*;do
  patch -p1 <$i
  mkdir -p $PKG/usr/doc/${NAME}-${VERSION}/patches/
  cp $i $PKG/usr/doc/${NAME}-${VERSION}/patches/
done
#-----------------------------------------------------
 
 

#SET PERMISSIONS
#-----------------------------------------
echo "Setting permissions..."
chown -R root:root .
find . -perm 664 -exec chmod 644 {} \;
find . -perm 777 -exec chmod 755 {} \;
find . -perm 2777 -exec chmod 755 {} \;
find . -perm 775 -exec chmod 755 {} \;
find . -perm 2755 -exec chmod 755 {} \;
find . -perm 774 -exec chmod 644 {} \;
find . -perm 666 -exec chmod 644 {} \;
find . -perm 600 -exec chmod 644 {} \;
find . -perm 444 -exec chmod 644 {} \;
find . -perm 400 -exec chmod 644 {} \;
find . -perm 440 -exec chmod 644 {} \;
find . -perm 511 -exec chmod 755 {} \;
find . -perm 711 -exec chmod 755 {} \;
find . -perm 555 -exec chmod 755 {} \;
#-----------------------------------------



#CONFIGURE & MAKE
#----------------------------------------------------------------------
# If you are building a KDE-related app, then change the following
# arguments in the script below:
# --prefix=$(kde-config -prefix) \
# --sysconfdir=/etc/kde \
#
# Making these changes will ensure that your package will build in the
# correct path and that it will work seamlessly within the KDE environment.
#
#-----------------------------------------------------------------------

echo "Configuring source..."

find $PKG -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

# Make a .desktop file
mkdir -p $PKG/usr/share/applications
cat $PKG/opt/teamviewer9/tv_bin/desktop/teamviewer-teamviewer9.desktop \
  | sed -e 's/EXEC/teamviewer/' -e 's/ICON/teamviewer/' \
  > $PKG/usr/share/applications/teamviewer.desktop

# Remove the dangling symlink first
rm $PKG/usr/bin/teamviewer

# Re-create the generic executable
( cd $PKG/usr/bin; ln -s /opt/teamviewer9/tv_bin/script/teamviewer teamviewer )

# Link icon to /usr/share/pixmaps
mkdir -p $PKG/usr/share/pixmaps
( ln -sf /opt/teamviewer9/tv_bin/desktop/teamviewer.png  $PKG/usr/share/pixmaps/teamviewer.png )

mkdir -p $PKG/usr/doc/$NAME-$VERSION
cat $CWD/$NAME.SlackBuild > $PKG/usr/doc/$NAME-$VERSION/$NAME.SlackBuild

# Move docs to official place
mv $PKG/opt/teamviewer9/doc/*.txt $PKG/usr/doc/$NAME-$VERSION
rm -rf $PKG/opt/teamviewer9/doc/

mkdir -p $PKG/etc/rc.d/
install -m 0644 $CWD/rc.teamviewerd $PKG/etc/rc.d/rc.teamviewerd.new

# Let's flip-flop the actual locations vs. symlinks of a couple of things:
# First remove the dangling symlink made by Debian packages:
rm -f $PKG/var/log/teamviewer $PKG/etc/teamviewer
mkdir -p $PKG/var/log/teamviewer $PKG/etc/teamviewer
rm -rf $PKG/opt/teamviewer9/logfiles/ $PKG/opt/teamviewer9/config/
ln -s /var/log/teamviewer $PKG/opt/teamviewer9/logfiles
ln -s /etc/teamviewer $PKG/opt/teamviewer9/config

#----------------------------------------------------------------------

if [ -d $PKG/usr/share/man ];then
mkdir -p $PKG/usr/man
mv $PKG/usr/share/man/* $PKG/usr/man
rm -rf $PKG/usr/share/man
fi
find $PKG/usr/man -type f -exec gzip -9 {} \;

if [ -d $PKG/usr/share/info ];then
mkdir -p $PKG/usr/info
mv $PKG/usr/share/info/* $PKG/usr/info
rm -rf $PKG/usr/share/info
fi 
find $PKG/usr/info -type f -exec gzip -9 {} \;

mkdir -p $PKG/install
if [ -d $PKG/usr/info ];then
cat >> $PKG/install/doinst.sh << EOF
CWD=\$(pwd)
cd usr/info
if [ -f dir ];then
    rm dir
fi
if [ -f dir.gz ];then
    rm dir.gz
fi
for i in *.info.gz;do
    install-info \$i dir
done
cd \$CWD
EOF
fi

# Add schemas install to the doinst.sh if schemas are found.
if [ -d $PKG/etc/gconf/schemas ];then
# Make sure we have gconftool installed
echo "if [ -x usr/bin/gconftool-2 ]; then" >> $PKG/install/doinst.sh
( cd $PKG/etc/gconf/schemas
for schema in *.schemas; do
 # Install schemas
 echo "GCONF_CONFIG_SOURCE=\"xml::etc/gconf/gconf.xml.defaults\" \
   usr/bin/gconftool-2 --makefile-install-rule \
   etc/gconf/schemas/${schema} >/dev/null 2>&1" \
   >> $PKG/install/doinst.sh
done;
)
# Finish off gconf block
echo "fi" >> $PKG/install/doinst.sh
fi
cat $CWD/doinst.sh >> $PKG/install/doinst.sh


#if there is a slack-desc in src dir use it
if test -f $CWD/slack-desc; then
cp $CWD/slack-desc $RELEASEDIR/slack-desc
else
# This creates the white space in front of "handy-ruler" in slack-desc below.

LENGTH=$(expr length "${NAME}")
SPACES=0
SHIM=""
until [ "$SPACES" = "$LENGTH" ]; do
SHIM="$SHIM "
let SPACES=$SPACES+1
done

# Fill in the package summary between the () below.
# Then package the description, License, Author and Website.
# There may be no more then 11 ${NAME}: lines in a valid slack-desc.

cat > $RELEASEDIR/slack-desc << EOF
# HOW TO EDIT THIS FILE:
# The "handy ruler" below makes it easier to edit a package description.  Line
# up the first '|' above the ':' following the base package name, and the '|'
# on the right side marks the last column you can put a character in.  You must
# make exactly 11 lines for the formatting to be correct.  It's also
# customary to leave one space after the ':'.

$SHIM|-----handy-ruler------------------------------------------------------|
${NAME}: ${NAME} ()
${NAME}:
${NAME}: 
${NAME}: 
${NAME}: 
${NAME}: 
${NAME}:
${NAME}:
${NAME}: License: GPL
${NAME}: Authors: 
${NAME}: Website: 

EOF
fi
cat >> $RELEASEDIR/slack-desc << EOF



#----------------------------------------
BUILDDATE: $(date)
PACKAGER:  $VL_PACKAGER
HOST:      $(uname -srm)
DISTRO:    $(cat /etc/vector-version)
CFLAGS:    $CFLAGS
LDFLAGS:   $LDFLAGS
CONFIGURE: $(awk "/\\$\ \.\/configure\ /" $TMP/$DIRNAME/config.log)

EOF

cat $RELEASEDIR/slack-desc > $PKG/install/slack-desc

#STRIPPING
#------------------------------------------------------------------------------------------------------------------
cd $PKG
echo " "
echo "Stripping...."
echo " "
find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
#------------------------------------------------------------------------------------------------------------------


#FINISH PACKAGE
#--------------------------------------------------------------
echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $RELEASEDIR $PKG
echo "Creating package ${NAME}-${VERSION}-$ARCH-$BUILD.txz"
makepkg -l y -c n $RELEASEDIR/${NAME}-${VERSION}-$ARCH-$BUILD.txz

cd $CWD
echo "Cleaning up temp files..." && rm -rf $TMP
echo "Package Complete"
fi
#--------------------------------------------------------------

# vim: set tabstop=4 shiftwidth=4 foldmethod=marker : ##
